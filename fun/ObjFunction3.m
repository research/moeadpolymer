function [f] = ObjFunction3(x, varargin)

[AAD_Gl, AAD_Gll] = RheologyFunction(x, varargin{:});

% calc objectives
f = zeros(4, 1);
f(1) = mean(AAD_Gl);
f(2) = mean(AAD_Gll);
f(3) = max(AAD_Gl);
f(4) = max(AAD_Gll);

return