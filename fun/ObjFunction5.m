function [f] = ObjFunction5(x, varargin)

[AAD_Gl, AAD_Gll, ~, ~, pairs, ~, ~] = RheologyFunction(x, varargin{:});

% calc objectives
f = zeros(3, 1);
f(1) = max(AAD_Gl);
f(2) = max(AAD_Gll);
f(3) = sum(pairs);

return