function [AAD_Gl, AAD_Gll, Gl_, Gll_, pairs, G, lambda] = RheologyFunction(x, type, varargin)

% decode chromosome
if type == 1 % adaptive number of pairs
    l = numel(x);
    n = l/3;
    G = power(10, x(1:3:l));
    lambda = power(10, x(2:3:l));
    pairs = (x(3:3:l) > 0);    
else % fixed number of pairs
    l = numel(x);
    n = l/2;
    G = power(10, x(1:2:l));
    lambda = power(10, x(2:2:l));
    pairs = true(n,1);    
end

% Gl and Gll are experimental values
% Gl_ and Gll_ are estimated values

global omegal omegall Gl Gll

% calc G'
m = numel(Gl); % number of data points
Gl_ = zeros(m, 1); % estimated values 
AAD_Gl = zeros(m, 1); % error - absolute deviation measure

for j = 1:m
    
    for i = 1:n
        if pairs(i)
            OmegaLambda = omegal(j)*lambda(i);            
            Gl_(j) = Gl_(j) + G(i)*OmegaLambda^2 / (1 + OmegaLambda^2);
        end
    end
    
    AAD_Gl(j) = abs( (Gl(j)-Gl_(j))/Gl(j) );
    
end

% calc G''
m = numel(Gll); % number of data points
Gll_ = zeros(m, 1); % estimated values 
AAD_Gll = zeros(m, 1); % error - absolute deviation measure

for j = 1:m
    
    for i = 1:n
        if pairs(i)
            OmegaLambda = omegall(j)*lambda(i);
            Gll_(j) = Gll_(j) + G(i)*OmegaLambda / (1 + OmegaLambda^2);
        end
    end
    
    AAD_Gll(j) = abs( (Gll(j)-Gll_(j))/Gll(j) );
    
end

return

