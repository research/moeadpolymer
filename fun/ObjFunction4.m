function [f] = ObjFunction4(x, varargin)

[AAD_Gl, AAD_Gll, ~, ~, pairs, ~, ~] = RheologyFunction(x, varargin{:});

% calc objectives
f = zeros(3, 1);
f(1) = mean(AAD_Gl);
f(2) = mean(AAD_Gll);
f(3) = sum(pairs);

return