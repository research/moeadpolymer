function [f] = ObjFunction2(x, varargin)

[AAD_Gl, AAD_Gll] = RheologyFunction(x, varargin{:});

% calc objectives
f = zeros(2, 1);
f(1) = max(AAD_Gl);
f(2) = max(AAD_Gll);

return