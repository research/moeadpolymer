function [Population_, Archive_] = MOEAD(Problem_, params, varargin)
%% Reference
% H. Li and Q. Zhang. Multiobjective optimization problems with complicated
% Pareto sets, MOEA/D and NSGA-II. IEEE Transactions on Evolutionary
% Computation, 13(2):284-302, 2009.
%% Input
% Problem.m - number of objective functions
% Problem.n - number of variables
% Problem.lb - column vector of lower bounds of variables
% Problem.ub - column vector of upper bounds of variables
% Problem.objFunction - objective function
%% Output
% A.x - approximation in the decision space
% A.f - approximation in the objective space
% A.g - inequality constraints values
% A.h - equality constraints values
% Statistics - structure with running data
%% Implementation
% Roman Denysiuk
%%

RandStream.setGlobalStream( RandStream('mt19937ar', 'Seed', 1000) );
% RandStream.setDefaultStream( RandStream('mt19937ar', 'Seed', 1000) );; % older version

global Problem Algorithm Population Archive Child

% problem structure
Problem = Problem_;
clear Problem_

% structure of algorithm parameters
Algorithm = struct('CR', 1.0, ... DE crossover probability
                   'F', 0.5, ... scaling parameter
                   'pc', 1.0, ... GA crossover probability
                   'etac', 20, ... GA crossover distribution
                   'pm', 1/Problem.n, ... mutation probability
                   'etam', 20, ... mutation distribution index
                   'T', 10, ... number of weight vectors in the neighborhood of each weight vector
                   'delta', 0.9, ... probability that the parent selected from the neighborhood
                   'nr', 1, ... maximum number of solutions replaced by each child solution
                   'mu', 100, ... population size
                   'maxGen', 5000, ... stopping criterion
                   'variation', 'GA', ... type of the variation operator (DE, GA, PM, GM, onePoint, twoPoint, uniform)
                   'fitness', 'WSFM', ... type of scalarizing fitness (WSM, CHB, PBI, VADS, WSFM);   
                   'normalization', true, ... normalization of objectives (false, true)     
                   'replacementType', 0, ... type of replacement (0 - original, 1 - global)
                   'verbose', false, ... vizualization (false, true)
                   'archive', false ... decide whether to maintain archive (false, true)
                   );

% input params
if nargin > 1 && ~isempty(params)
    if isfield(params, 'seed')
        RandStream.setGlobalStream( RandStream('mt19937ar', 'Seed', params.seed) );
        %RandStream.setDefaultStream( RandStream('mt19937ar', 'Seed', params.seed) ); % older version
    end
    if isfield(params, 'popFile')
        initial = load(params.popFile);
    end
    for str = {'mu' 'maxGen' 'variation' 'fitness' 'verbose'}
        if isfield(params, str{1})
            Algorithm.(str{1}) = params.(str{1});
        end
    end
end
                
% init archive
if Algorithm.archive    
Archive = struct('x', zeros(Algorithm.mu+1, Problem.n), ... decision vector
                 'f', zeros(Algorithm.mu+1, Problem.m), ... objective vector
                 'ideal', [], ... ideal vector
                 'nadir', [], ... nadir vector
                 'capacity', Algorithm.mu+1, ... max capacity of archive
                 'size', 0, ... current archive size
                 'allocated', false(Algorithm.mu+1, 1) ... indicates whether slot is allocated
                 );
end

% individual in the population
Individual = struct('x', zeros(Problem.n, 1), ... decision vector
                    'f', zeros(Problem.m, 1) ... objective vector
                    );
              
% initialize population structure
Population = repmat(Individual, Algorithm.mu, 1);
Child = repmat(Individual, 1, 1);

% randomly generate initial population
for i = 1:Algorithm.mu
    if isempty(Problem.lb) || isempty(Problem.ub)
        Population(i).x = round(rand(Problem.n,1));
    else
        Population(i).x = Problem.lb + rand(Problem.n,1).*(Problem.ub - Problem.lb);
    end
    [Population(i)] = Evaluation(Population(i), varargin{:});
end

% savePopulation(0);

% initialize weight vectors
if Problem.m == 2
    % generate
    H = Algorithm.mu-1;
    t = [0:H]';
    w = [t H-t]/H;
    Algorithm.w = w';
else
    % read from file
    load weights
    Algorithm.w = weights{Problem.m, 1+Algorithm.mu/100}';
end
Algorithm.w(Algorithm.w==0) = 1e-4;

% initialize neighborhood matrix
Algorithm.neighborhood = zeros(Algorithm.mu, Algorithm.T);
for i = 1:Algorithm.mu
    [~, index] = sort( sqrt( sum( power(repmat(Algorithm.w(:,i),1,Algorithm.mu)-Algorithm.w, 2) ,1) ) );    
    Algorithm.neighborhood(i,:) = index(1:Algorithm.T);
end
clear index weights

Algorithm.pop = 1:Algorithm.mu;

% reference point
Algorithm.ideal = min([Population.f], [], 2);

% vizualize current population
global ptr_plot
ptr_plot = [];
if (Algorithm.verbose)
    Visualization([Population.f], 0, varargin{:});
end

% some run vars
global pool poolSize currentSubproblem

%% start evolutionary process
for gen = 1:Algorithm.maxGen
    
    for currentSubproblem = 1:Algorithm.mu
        
        
        % MATING SELECTION (select individuals from neighborhood or population)
        if rand <= Algorithm.delta
            pool = Algorithm.neighborhood(currentSubproblem,:);
            poolSize = Algorithm.T;
        else
            pool = Algorithm.pop;
            poolSize = Algorithm.mu;
        end
        
        
        % VARIATION (produce offspring)
        switch Algorithm.variation
            
            case 'DE' 
                DEoperator(varargin{:}); % differential evolution operator
                PolynomialMutation(varargin{:}); % polynomial mutation
                
            case 'GA' 
                SBXcrossover(varargin{:}); % genetic operator (SBX crossover)
                PolynomialMutation(varargin{:}); % polynomial mutation
                
            case 'PM' 
                ProbabilisticModel(varargin{:}); % probabilistic model based operator
                PolynomialMutation(varargin{:}); % polynomial mutation
                
            case 'GM' 
                GuidedMutation(varargin{:}); % guided mutation
                PolynomialMutation(varargin{:}); % polynomial mutation
                
            case 'onePoint'
                OnePointCrossover(varargin{:}); % one point crossover
                BitFlipMutation(varargin{:}); % bit flip mutation
                
            case 'twoPoint'
                TwoPointCrossover(varargin{:}); % two point crossover
                BitFlipMutation(varargin{:}); % bit flip mutation
                
            case 'uniform'
                UniformCrossover(varargin{:}); % uniform crossover
                BitFlipMutation(varargin{:}); % bit flip mutation
                
        end   

        
        % evaluate a new solution
        [Child] = Evaluation(Child, varargin{:});
        
        
        % UPDATE
        % update ideal point
        Algorithm.ideal = zeros(size(Child.f)); % min(Algorithm.ideal, Child.f); % 
        if Algorithm.normalization
            Algorithm.nadir = max([[Population.f] Child.f], [], 2);
        end
        
                
        % ENVIRONMENTAL SELECTION (update population)
        
        % find replacement pool
        if poolSize == Algorithm.mu
            replacePool = pool;
        elseif Algorithm.replacementType == 0
            replacePool = Algorithm.neighborhood(currentSubproblem,:);
        elseif Algorithm.replacementType == 1
            replacePool = Algorithm.neighborhood(findMatchingSubproblem(), :);
        end
        replacePool = replacePool(randperm(poolSize));        
            
        c = 0;        
        for j = replacePool
            
            % calculate fitness
            if Algorithm.normalization
                newFitness = Fitness(zeros(Problem.m, 1), Algorithm.w(:,j), Normalization(Child.f, Algorithm.ideal, Algorithm.nadir), Algorithm.fitness);
                oldFitness = Fitness(zeros(Problem.m, 1), Algorithm.w(:,j), Normalization(Population(j).f, Algorithm.ideal, Algorithm.nadir), Algorithm.fitness);
            else
                newFitness = Fitness(Algorithm.ideal, Algorithm.w(:,j), Child.f, Algorithm.fitness);
                oldFitness = Fitness(Algorithm.ideal, Algorithm.w(:,j), Population(j).f, Algorithm.fitness);
            end
            
            % replace
            if newFitness < oldFitness
                Population(j) = Child;
%                 Population(j).x = Child.x;
%                 Population(j).f = Child.f;
                c = c + 1;
            end
            if c == Algorithm.nr
                break
            end
            
        end
        
    end
    
    % vizualize current population    
    if Algorithm.verbose
        if Algorithm.archive
            Visualization(Archive.f(Archive.allocated,:)', gen, varargin{:});
        else 
            Visualization([Population.f], gen, varargin{:});
        end        
    end
    
%     if mod(gen, 100) == 0
%         savePopulation(gen);
%     end
    
end
savePopulation(5000);
% output population
Population_.x = [Population.x]';
Population_.f = [Population.f]';

% output archive
if Algorithm.archive
    Archive_.x = Archive.x(Archive.allocated,:);
    Archive_.f = Archive.f(Archive.allocated,:);
else
    Archive_ = [];
end

return


function savePopulation(gen, varargin)

global Population
A.x = [Population.x]';
A.f = [Population.f]';
save(sprintf('pop_%d', gen), 'A');

return


%% Visualization Procedure
function Visualization(data, gen, varargin)

global ptr_plot

if isrow(data);
    data = data(:);
end

if isempty(ptr_plot)
    title(sprintf('Population at generation: %d', gen));
    xlabel('f_1', 'FontSize', 16, 'FontName', 'Times New Roman');
    ylabel('f_2', 'FontSize', 16, 'FontName', 'Times New Roman'); 
    grid on;
    drawnow; 
    hold on;
    ptr_plot = plot(data(1,:), data(2,:), 'r.', 'MarkerSize', 8, 'LineWidth', 1);
else
    pause(0.01)
    title(sprintf('Population at generation: %d', gen));
    set(ptr_plot, 'XData', data(1,:), 'YData', data(2,:), 'color', 'r');
    drawnow;
end

return


%% findSubproblem - find best matching subproblem for offspring
function [idx] = findMatchingSubproblem(varargin)

global Problem Algorithm Child 

idx = 0;
best = inf;

for j = 1:Algorithm.mu
    % calculate fitness
    if Algorithm.normalization
        value = Fitness(zeros(Problem.m, 1), Algorithm.w(:,j), Normalization(Child.f, Algorithm.ideal, Algorithm.nadir), Algorithm.fitness);
    else
        value = Fitness(Algorithm.ideal, Algorithm.w(:,j), Child.f, Algorithm.fitness);
    end
    if value < best
        idx = j;
        best = value;
    end
end

return


%% genetic operator onePointCrossover
function OnePointCrossover(varargin)

global Problem Algorithm Population Child 

n = Problem.n;
pc = Algorithm.pc;

% select individuals for reproduction
parents = uniformSelection(2);
p1 = parents(1);
p2 = parents(2);

% decide whether to apply crossover
flag = false; 
if rand <= pc
    flag = true;    
end

% generate crossover point
point = randi(n);

% apply one point crossover
for j = 1:n
    if j >= point && flag
        Child.x(j) = Population(p2).x(j);
    else
        Child.x(j) = Population(p1).x(j);
    end
end

return


%% genetic operator twoPointCrossover
function TwoPointCrossover(varargin)

global Problem Algorithm Population Child 

n = Problem.n;
pc = Algorithm.pc;

% select individuals for reproduction
parents = uniformSelection(2);
p1 = parents(1);
p2 = parents(2);

% generate crossover points
point1 = randi(n);
point2 = randi(n);
while point2 == point1
    point2 = randi(n);
end

if point2 < point1
    temp = point1;
    point1 = point2;
    point2 = temp;
end

% decide whether to apply crossover
flag = false; 
if rand <= pc
    flag = true;    
end

% apply one point crossover
for j = 1:n
    if point1 <= j && j <= point2  && flag
        Child.x(j) = Population(p2).x(j);
    else
        Child.x(j) = Population(p1).x(j);
    end
end

return


%% genetic operator twoPointCrossover
function UniformCrossover(varargin)

global Problem Algorithm Population Child 

n = Problem.n;
pc = Algorithm.pc;

% select individuals for reproduction
parents = uniformSelection(2);
p1 = parents(1);
p2 = parents(2);

% decide whether to apply crossover
flag = false; 
if rand <= pc
    flag = true;    
end

% apply uniform crossover
for j = 1:n
    if rand < 0.5 && flag
        Child.x(j) = Population(p2).x(j);
    else
        Child.x(j) = Population(p1).x(j);
    end
end

return


%% bit flip mutation
function BitFlipMutation(varargin)

global Problem Algorithm Child 

n = Problem.n;
pm = Algorithm.pm;

% bit flip mutation
for j = 1:n
    if rand < pm
        Child.x(j) = double(~Child.x(j));
    end
end

return


%% differential evolution operator
function DEoperator(varargin)

global Problem Algorithm Population Child
global pool poolSize currentSubproblem

% select individuals for reproduction
r1 = currentSubproblem;
% r1 = pool(randi(poolSize));
r2 = pool(randi(poolSize));
while r2 == r1
    r2 = pool(randi(poolSize));
end
r3 = pool(randi(poolSize));
while (r3 == r1 || r3 == r2)
    r3 = pool(randi(poolSize));
end

% DE operator
jr = randi(Problem.n);
for j = 1:Problem.n
    if (rand <= Algorithm.CR || j == jr)
        Child.x(j) = Population(r1).x(j) + Algorithm.F*(Population(r2).x(j) - Population(r3).x(j));
    else
        Child.x(j) = Population(r1).x(j);
    end
    % bounds
    if Child.x(j) < Problem.lb(j)
        Child.x(j) = Problem.lb(j) + rand*(Population(r1).x(j)-Problem.lb(j));
    elseif Child.x(j) > Problem.ub(j)
        Child.x(j) = Problem.ub(j) - rand*(Problem.ub(j) - Population(r1).x(j));
    end
end

return


%% uniform parent selection
function [parents] = uniformSelection(numParents, varargin)

global pool poolSize

parents = zeros(1, numParents);
for i = 1:numParents
   p =  pool(randi(poolSize));
   while any(ismember(p, parents))
       p =  pool(randi(poolSize));
   end
   parents(i) = p;
end

return



%% genetic operator (SBX crossover)
function SBXcrossover(varargin)

global Problem Algorithm Population Child

% select individuals for reproduction
parents = uniformSelection(2);
p1 = parents(1);
p2 = parents(2);

% initialize offspring
parent_1 = Population(p1).x;
parent_2 = Population(p2).x;

% SBX crossover
pc = Algorithm.pc;
etac = Algorithm.etac;

% apply SBX (Simulated Binary Crossover) with probability pc
if rand <= pc
    
    % perform crossover
    for j = 1:Problem.n
        
        % calculate beta
        u = rand;
        if u <= 0.5
            beta = power(2.0*u, 1.0/(etac+1.0));
        else
            beta = power(1.0/(2.0 - 2.0*u), 1.0/(etac+1.0));
        end
        
        % calculate Children gens, swaping them with probability 0.5
        if rand <= 0.5
            Child.x(j) = 0.5*((1 + beta)*parent_1(j) + (1 - beta)*parent_2(j));
        else
            Child.x(j) = 0.5*((1 - beta)*parent_1(j) + (1 + beta)*parent_2(j));
        end
    end
    
end

return


%%
function ProbabilisticModel(varargin)

global Problem Population Child
global pool poolSize currentSubproblem

% build a model
data = cell2mat({Population(pool).x})';
data = data - repmat(mean(data), poolSize, 1);
C = data'*data/(poolSize-1);
[B, D] = eig(C);

% generate offspring
Child.x = Population(currentSubproblem).x + B*(sqrt(diag(abs(D))).*randn(Problem.n, 1));

return


%% guided mutation
function GuidedMutation(varargin)

global Problem Algorithm Population Child
global pool poolSize currentSubproblem

% select a neighbor
r = pool(randi(poolSize));
while (r == currentSubproblem)
    r = pool(randi(poolSize));
end

t = Population(currentSubproblem).x; % current subproblem
x = Population(r).x; % neighbor

H = zeros(Problem.n, 1);
for j = 1:Problem.n
    if rand <= Algorithm.pm
        H(j) = randn;
    end
end

mu = 0.005;
R = max(0.1*norm(t-x), mu);

% produce Child
Child.x = x + 0.5*(t-x)*randn + R*H;

return


%% polynomial mutation
function PolynomialMutation(varargin)

global Problem Algorithm Child

% params
% persistent n lb ub pm etam
% if isempty(n)
    n = Problem.n;
    lb = Problem.lb;
    ub = Problem.ub;
    pm = Algorithm.pm;
    etam = Algorithm.etam;
% end

% apply polynomial mutation
for j = 1:n
    
    % polynomial mutation applied with probability pm
    if rand <= pm
        u = rand;
        if u <= 0.5
            delta = power(2.0*u, 1.0/(etam+1.0)) - 1.0;
        else
            delta = 1.0 - power(2.0 - 2.0*u, 1.0/(etam+1.0));
        end
        Child.x(j) = Child.x(j) + delta*(ub(j) - lb(j));
    end
    % bounds
    if Child.x(j) < lb(j),
        Child.x(j) = lb(j);
    end
    if Child.x(j) > ub(j)
        Child.x(j) = ub(j);
    end
    
end

return


%% fitness function
function value = Fitness(z, w, f, type)

switch type
    
    case 'WSM' % weighted sum scalariazing methd
        value = w'*(f-z);
        
    case 'CHB' % Chebyshev method
        value = max(w.*abs(f-z));
        
    case 'PBI' % penalty boundary intersection method
        d1 = (w'*(f-z))/norm(w);
        d2 = norm(f - (z+d1*w));
        theta = 5;
        value = d1 + theta*d2;
        
    case 'VADS' % vector-angle distance scaling aggregation method (MSOPS-II)
        b = 100;
        value = exp((b+1)*log(norm(f)) - b*log(w'*(f-z)));
        
    case 'WSFM' % Weighted Stress Function Method
        value = weightedStressFunctionMethod(f, w);
end

return


function [metric] = weightedStressFunctionMethod(f, w)

% get size
m = numel(f);

% f = max(1 - f, 0.001); % my for minimization use
% w = max(1 - w, 0.001); % for shape

f = 1 - f; % my for minimization use
f(f==0) = 0.001;

w = 1 - w; % for shape
w(w==0) = 0.001;

delta1 = 0.001;
delta2 = 0.001;
        
sigma = zeros(1, m);

% calc metric values   
for j = 1:m
    
    xi = -1/tan(-pi/(2+2*delta2))*tan(pi*(w(j)-0.5)/(1+delta2)) + 1;
    phi = 3/4*power(1-w(j), 2) + 2*(1-w(j))+delta1;
    psi = phi + 4*w(j) - 2;

    if(f(j) <= w(j))
        omega =   -xi*psi/(tan(pi/phi*(w(j)-1)+delta1)*phi);
        
        sigma(j) = omega*tan(-pi/psi*(f(j) - w(j))) + xi;
        
    else
        omega = -xi/tan(pi/phi*(w(j)-1));
        
        sigma(j) = omega*tan(-pi/phi*(f(j)-w(j))) + xi;
    end
   
end

metric = max(sigma);

return


%% Normalization Procedure
function [f] = Normalization(f, ideal, nadir)

global  Archive Algorithm

f = (f - ideal)./(nadir-ideal);

% f = (f - Archive.ideal(:))./transpose(Archive.nadir-Archive.ideal);

return


%% Evaluation Procedure
function [individual] = Evaluation(individual, varargin)

global Problem Algorithm

% [individual.f, individual.x] = feval(Problem.objFunction, individual.x, varargin{:});
% [individual.f] = feval(Problem.objFunction, individual.x, varargin{:});
[individual.f] = feval(Problem.objFunction, individual.x, Problem.type, varargin{:});

if Algorithm.archive
    % add to archive
    UpdateArchive(individual.x', individual.f');
end

return


%%
function UpdateArchive(vars, objs, varargin)

global Problem Archive

toAccept = true;

for i = 1:Archive.capacity
    
    if Archive.allocated(i) % slot in the archive is not empty
        
        % init
        Archive.allocated(i) = false; % member is dominated
        toAccept = false; % candidate is dominated
        
        % check
        for j = 1:Problem.m
            if Archive.f(i,j) < objs(j)
                Archive.allocated(i) = true; % member is nondominated
            end
            if Archive.f(i,j) > objs(j)
                toAccept = true; % candidate is nondominated
            end
        end
        
        % if both are equal (archive member remain)
        if ~any([Archive.allocated(i) toAccept])            
            Archive.allocated(i) = true;
            break
        end
        
        if ~Archive.allocated(i)
            Archive.size = Archive.size - 1; % update arc size
        end
        
        if ~toAccept
            break; % candidate does not enter into archive
        end
    end
    
end

if toAccept  
    % find first available slot
    idx = find(Archive.allocated == false);
    idx = idx(1);
    
    % add to archive
    Archive.x(idx,:) = vars;
    Archive.f(idx,:) = objs;
    Archive.allocated(idx) = true;
    
    % update size
    Archive.size = Archive.size + 1;
    
    % update bounds
    Archive.ideal = min(Archive.f(Archive.allocated,:));
    Archive.nadir = max(Archive.f(Archive.allocated,:));
end

if Archive.size == Archive.capacity
    hv = HypervolumeContribution(Archive.f, Archive.ideal, Archive.nadir);
    % find smallest hv contribution
    %     if Problem.m <= 7
    %         hv = hv_contribution_mex(Normalization()', min(Problem.m, 4));
    %     else
    %         hv = hv_contribution_mex(Normalization()', [4; 0; 1; 1e4]);
    %     end
    %[~, extrem] = max(Archive.f);
    %hv(extrem) = 1;
    [~, idx] = min(hv);
    Archive.allocated(idx) = false; % remove memebr having smallest hv contribution
    Archive.size = Archive.size - 1;
end

return


%% hypervolume contribution matlab computation
function [s] = HypervolumeContribution(f, ideal, nadir, varargin)

% get size
[K, M] = size(f);

% scale each objective to the interval [0,1]
f = (f - repmat(ideal, K, 1))./repmat(nadir-ideal, K, 1);

% init s metric values
s = zeros(K, 1);

if M == 2
    
    % two objective case
    [~, idx] = sort(f(:,1));
    for i = 2:K-1
        s(idx(i)) = (f(idx(i+1),1)-f(idx(i),1))*(f(idx(i-1),2)-f(idx(i),2));
    end
    s([idx(1) idx(end)]) = 1;% inf;
    
elseif M == 3
    
    % three objective case
    a=sort(f(:,1));
    b=sort(f(:,2));
    r = [1.1 1.1 1.1]; %max(f)+1;
    a=[a;r(1)];
    b=[b;r(2)];
    
    best1_f3=r(3)*ones(K);
    best2_f3=r(3)*ones(K);
    
    for i=1:K
        for j=1:K
            for k=1:K
                if f(k,1)<=a(i) && f(k,2)<=b(j)
                    % s_k dominates cell (i,j) conc. f1, f2
                    if f(k,3)<best1_f3(i,j)
                        best2_f3(i,j)=best1_f3(i,j);
                        best1_f3(i,j)=f(k,3);
                    elseif f(k,3)<best2_f3(i,j) && f(k,3)>best1_f3(i,j)
                        best2_f3(i,j)=f(k,3);
                    end
                end
            end
        end
    end
    
    for i=1:K
        for j=1:K
            ownerNumber=0;
            owner=0;
            for k=1:K
                if f(k,1)<=a(i) && f(k,2)<=b(j) && best1_f3(i,j)==f(k,3)
                    % s_k dominates cell (i,j) conc. f1, f2
                    ownerNumber=ownerNumber+1;
                    owner=k;
                end
            end
            if ownerNumber==1
                % cell (i,j) is dominated disjoint
                s(owner)=s(owner)+(a(i+1)-a(i))*(b(j+1)-b(j))*(best2_f3(i,j)-best1_f3(i,j));
            end
        end
    end
    
elseif M > 3 && M < 7
    
    % set ref point
    r = 1.1*ones(1, M);
    
    % calc all hypervolume
    hv = hypervolume_mex(f, r);
    
    % calc contributions
    idx = true(K, 1);
    for i = 1:K
        if any(f(i,:) == 1) % set max hv to extreme solutions
            s(i) = 1; % inf;
        else
            idx(i) = false;
            s(i) = hv - hypervolume_mex(f(idx,:), r);
            idx(i) = true;
        end
    end
end

return
