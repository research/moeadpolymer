function [f] = ObjFunction1(x, varargin)

[AAD_Gl, AAD_Gll] = RheologyFunction(x, varargin{:});

% calc objectives
f = zeros(2, 1);
f(1) = mean(AAD_Gl);
f(2) = mean(AAD_Gll);

return