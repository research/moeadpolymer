function [f] = ObjFunction7(x, varargin)

[AAD_Gl, AAD_Gll, ~, ~, pairs, ~, ~] = RheologyFunction(x, varargin{:});

% calc objectives
f = zeros(2, 1);
f(1) = (mean(AAD_Gl) + mean(AAD_Gll))/2;
f(2) = sum(pairs);

return