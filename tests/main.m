clear all
clear
clc

addpath(genpath('../fun'));
addpath(genpath('../input'));

%% general parameters

numRuns = 25;
seeds = 1000:1000:25000;
% numObjs = [2 2 4 3 3 5 2];

generations = 0:500:5000;

material = 'LDPE'; % LDPE PS
study = 'objectives';

switch study
    case 'variation'
        variants = {'GA' 'DE' 'PM'};
    case 'fitness'
        variants = {'WSFM' 'CHB' 'PBI'};
    case 'objectives'
        variants = {'objs_1' 'objs_2' 'objs_3' 'objs_4' 'objs_5' 'objs_6' 'objs_7'};
    case 'pairs'
        variants = {'pairs_1' 'pairs_2' 'pairs_3' 'pairs_4' 'pairs_5' 'pairs_6' 'pairs_7' 'pairs_8' 'pairs_9' 'pairs_10' 'pairs_11' 'pairs_12' 'pairs_13' 'pairs_14' 'pairs_15' 'pairs_16' 'pairs_17' 'pairs_18' 'pairs_19' 'pairs_20' 'pairs_adapt'};
end


%% define material

global omegal omegall Gl Gll

switch material
    case 'LDPE'
        omegal = xlsread('data.xlsx', 2, 'A4:A59');
        Gl = xlsread('data.xlsx', 2, 'B4:B59');
        omegall = xlsread('data.xlsx', 2, 'C4:C60');
        Gll = xlsread('data.xlsx', 2, 'D4:D60');
    case 'PS'
        omegal = xlsread('data.xlsx', 1, 'A4:A107');
        Gl = xlsread('data.xlsx', 1, 'B4:B107');
        omegall = xlsread('data.xlsx', 1, 'A4:A112');
        Gll = xlsread('data.xlsx', 1, 'C4:C112');
end


%% multiple runs for different studies

for k = 4%1:numel(variants)
    
    variant = variants{k};
    
    % define params for MOEA
    switch study
        case 'variation'
            params = struct('mu', 100, 'maxGen', 5000, 'variation', variant, 'fitness', 'WSFM', 'seed', 1);
        case 'fitness'
            params = struct('mu', 100, 'maxGen', 5000, 'variation', 'GA', 'fitness', variant, 'seed', 1);
        otherwise
            params = struct('mu', 100, 'maxGen', 5000, 'variation', 'GA', 'fitness', 'WSFM', 'seed', 1);
    end
    
    % define problem structure
    if strcmp(study, 'pairs')
        
        if strcmp(variant(7:end), 'adapt')
            Problem.type = 1; % 0 - fixed number of pairs; 1 - adaptive number of pairs;
            nPairs = 20; % maximum number of pairs
            Problem.n = 3*nPairs; % number of variables
        else
            Problem.type = 0; % 0 - fixed number of pairs; 1 - adaptive number of pairs;
            nPairs = str2double(variant(7:end)); % maximum number of pairs
            Problem.n = 2*nPairs; % number of variables
        end
        
        Problem.objFunction = 'ObjFunction1'; % problem name
        Problem.m = 2; % number of objective functions
        Problem.lb(1:3:Problem.n,1) = -4; Problem.lb(2:3:Problem.n,1) = -6; Problem.lb(3:3:Problem.n,1) = -1; % lower bounds
        Problem.ub(1:3:Problem.n,1) = 10; Problem.ub(2:3:Problem.n,1) = 6;  Problem.ub(3:3:Problem.n,1) = 1; % upper bounds        
        
    else
        
        Problem.objFunction = 'ObjFunction4'; %'ObjFunction1'; % problem name
        Problem.m = 3; % number of objective functions
        Problem.type = 1; % 0 - fixed number of pairs; 1 - adaptive number of pairs;
        nPairs = 20; % maximum number of pairs
        Problem.n = 3*nPairs; % number of variables
        Problem.lb(1:3:Problem.n,1) = -4; Problem.lb(2:3:Problem.n,1) = -6; Problem.lb(3:3:Problem.n,1) = -1; % lower bounds
        Problem.ub(1:3:Problem.n,1) = 10; Problem.ub(2:3:Problem.n,1) = 6;  Problem.ub(3:3:Problem.n,1) = 1; % upper bounds
        
    end
    
    for j = 1:numRuns
        
        % define parameters
        params.seed = seeds(j);      
        
        [A] = MOEAD(Problem, params); % run moea
        
        folder = sprintf('../results/material_%s/study_%s/%s/seed_%d', material, study, variant, params.seed);
        if exist(folder,'dir') ~= 7;
            mkdir(folder);
        end
        
        for g = 0:100:5000
            SOURCE = sprintf('pop_%d.mat', g);
            if exist(SOURCE, 'file') > 0
                DESTINATION = sprintf('%s/%s', folder, SOURCE);
                
                movefile(SOURCE, DESTINATION, 'f');
            end
        end
        
        fprintf('material: %s\t study: %s\t variant: %s\t run: %d\n', material, study, variant, params.seed);
        
    end
    
end


%% adaptive with different limits for num of pairs (additional tests)

% for j = 1:numRuns
%     
%     for nPairs = 3:10
%         
%         % define params for MOEA
%         params = struct('mu', 100, 'maxGen', 5000, 'variation', 'GA', 'fitness', 'WSFM', 'seed', seeds(j));
%         
%         % define problem structure
%         Problem.objFunction = 'ObjFunction1'; % problem name
%         Problem.m = 2; % number of objective functions
%         Problem.type = 1; % 0 - fixed number of pairs; 1 - adaptive number of pairs;
%         Problem.n = 3*nPairs; % number of variables
%         Problem.lb(1:3:Problem.n,1) = -4; Problem.lb(2:3:Problem.n,1) = -6; Problem.lb(3:3:Problem.n,1) = -1; % lower bounds
%         Problem.ub(1:3:Problem.n,1) = 10; Problem.ub(2:3:Problem.n,1) = 6;  Problem.ub(3:3:Problem.n,1) = 1; % upper bounds     
%         
%         folder = sprintf('../results/material_%s/study_%s/max_%d/seed_%d', material, 'maxpairs', nPairs, params.seed);
%         if exist(folder,'dir') > 0;
%             continue
%         else
%             mkdir(folder);
%         end
%         
%         [A] = MOEAD(Problem, params); % run moea
%         
%         for g = 0:100:5000
%             SOURCE = sprintf('pop_%d.mat', g);
%             if exist(SOURCE, 'file') > 0
%                 DESTINATION = sprintf('%s/%s', folder, SOURCE);
%                 
%                 movefile(SOURCE, DESTINATION, 'f');
%             end
%         end
%         
%         fprintf('material: %s\t study: %s\t variant: %d\t run: %d\n', material, 'maxpairs', nPairs, params.seed);
%         
%     end
%     
% end
